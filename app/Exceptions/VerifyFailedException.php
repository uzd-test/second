<?php


namespace App\Exceptions;


class VerifyFailedException extends \Exception
{
    const CODE_USER_NOT_FOUND = 1;
    const CODE_ALREADY_VERIFIED = 2;
    const CODE_INVALID_TOKEN = 3;
}
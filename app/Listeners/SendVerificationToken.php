<?php

namespace App\Listeners;

use App\Mail\VerificationEmail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Str;

class SendVerificationToken
{
    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * Create the event listener.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        //
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle($event)
    {
        $user = $event->user;

        $user->verify_token = Str::random(60);

        $user->save();

        $this->mailer
            ->to($user->email)
            ->send(new VerificationEmail($user));
    }
}

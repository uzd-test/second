<?php

namespace App\Services;


use App\Services\RssFeed\Article;
use App\Services\RssFeed\Channel;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Config\Repository as Config;
use Illuminate\Contracts\Cache\Repository as Cache;
use Psr\Log\LoggerInterface;

class RssFeedService
{
    const CONFIG_PREFIX = 'feed';

    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var Cache
     */
    protected $cache;
    /**
     * @var Config
     */
    protected $config;

    /**
     * RssService constructor.
     *
     * @param LoggerInterface $logger
     * @param Cache           $cache
     * @param Config          $config
     */
    public function __construct(LoggerInterface $logger, Cache $cache, Config $config)
    {
        $this->logger = $logger;
        $this->cache = $cache;
        $this->config = $config;
    }


    /**
     * Parses RSS Feed
     *
     * @param $response
     *
     * @return Channel|bool
     */
    public function parseChannel($response)
    {
        $articles = collect();

        try {
            $dom = new \DOMDocument();
            $dom->loadXML($response);

            $channelNodes = $dom->getElementsByTagName('channel');

            if ($channelNodes->count() === 0) {
                return null;
            }

            $channelNode = $channelNodes->item(0);

            $channel = new Channel();

            foreach ($channelNode->getElementsByTagName('*') as $node) {
                $name = $node->nodeName;

                // Only care about the direct descendants
                if ($node->parentNode !== $channelNode && $node->parentNode->nodeName !== "image") {
                    continue;
                }

                // Don't need articles here
                if ($name === "item") {
                    continue;
                }

                if ($node->parentNode !== $channelNode) {
                    $parent = $node->parentNode->nodeName;

                    $sub = (array)$channel->get($parent, []);

                    $sub[$node->nodeName] = $node->nodeValue;

                    $channel[$parent] = $sub;
                } else if ($node->childNodes->count() === 1) {
                    $channel[$name] = $node->nodeValue;
                }
            }

            $channel->lastBuildDate = $channel->lastBuildDate ? Carbon::parse($channel->lastBuildDate) : null;

            $root = $dom->documentElement;

            $imageNs = $root->getAttribute('xmlns:media');
            $slashNs = $root->getAttribute('xmlns:slash');


            $articleNodes = $dom->getElementsByTagName('item');

            /**
             * @var $articleNode \DOMElement
             */
            foreach ($articleNodes as $articleNode) {
                $article = new Article();

                foreach ($articleNode->getElementsByTagName('*') as $item) {
                    if (!$item->prefix && $item->parentNode === $articleNode) {
                        $article[$item->tagName] = $item->nodeValue;
                    }
                }

                $image = null;
                $comments = null;

                if ($imageNs) {
                    $imageNode = $articleNode->getElementsByTagNameNS($imageNs, 'content')->item(0);
                    $image = $imageNode ? $imageNode->getAttribute('url') : null;
                }

                if ($slashNs) {
                    $commentsNode = $articleNode->getElementsByTagNameNS($slashNs, 'comments')->item(0);
                    $comments = $commentsNode ? $commentsNode->nodeValue : null;
                }

                $article->pubDate = Carbon::parse($article->pubDate);

                $article->imageUrl = $image;
                $article->commentCount = $comments;

                $articles->push($article);
            }

            $channel->articles = $articles;
        } catch (\Throwable $e) {
            $this->logger->error("Feed: Failed to parse news items", [$e]);

            return false;
        }

        return $channel;
    }

    /**
     * Returns parsed channel data
     *
     * @param     $channelName
     * @param int $cache
     *
     * @return Channel|bool
     */
    public function getChannel($channelName = null, $cache = 60)
    {
        $key = "feed_" . sha1($this->getFeedUrl() . $channelName);

        if ($cache === 0) {
            return $this->parseChannel($this->requestChannel($channelName));
        }

        $response = $this->cache->remember($key, $cache, function () use ($channelName) {
            return $this->requestChannel($channelName);
        });

        return $this->parseChannel($response);
    }

    /**
     * Sends request to RSS feed
     *
     * @param string|null|false $channel null to use default channel, false to use no channel
     *
     * @return bool|string
     */
    public function requestChannel($channel = null)
    {
        try {
            $client = new Client();

            $qs = null;

            if($channel === null) {
                $channel = $this->getDefaultChannelName();
            }

            if($channel) {
                $qs = compact('channel');
            }

            $response = $client->request("GET", $this->getFeedUrl(), [
                'query' => $qs
            ]);

            if ($response->getStatusCode() !== 200) {
                $this->logger->error("Feed: Invalid response code {$response->getStatusCode()} provided.", [
                    "url" => $this->getFeedUrl(),
                    "query" => $qs
                ]);

                return false;
            }

            return $response->getBody()->getContents();
        } catch (\Throwable $e) {
            $this->logger->error("Feed: Request failed {$e->getMessage()}", [$e]);

            return false;
        }
    }

    /**
     * @return null|string
     */
    public function getDefaultChannelName() :? string
    {
        return $this->getConfig("channel");
    }


    /**
     * @return string
     */
    public function getFeedUrl(): string
    {
        return $this->getConfig("url");
    }

    /**
     * @param null $key
     *
     * @return mixed
     */
    protected function getConfig($key = null)
    {
        $prefix = static::CONFIG_PREFIX;

        if($key === null) {
            return $this->config->get($prefix);
        }

        return $this->config->get("{$prefix}.{$key}");
    }
}
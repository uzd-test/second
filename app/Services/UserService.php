<?php


namespace App\Services;


use App\Events\UserVerified;
use App\Exceptions\VerifyFailedException;
use App\User;

class UserService
{
    /**
     * Verifies user
     *
     * @param User|int $user
     *
     * @return bool
     * @throws VerifyFailedException
     */
    public function verify($user, $token)
    {
        if(!($user instanceof User)) {
            $user = User::find($user);
        }

        if($user === null) {
            throw new VerifyFailedException(__("User not found"), VerifyFailedException::CODE_USER_NOT_FOUND);
        }

        if($user->verified === true) {
            throw new VerifyFailedException(__("User already verified"), VerifyFailedException::CODE_ALREADY_VERIFIED);
        }

        if($user->verify_token !== $token) {
            throw new VerifyFailedException(__("Invalid verification token"), VerifyFailedException::CODE_INVALID_TOKEN);
        }

        $user->verified = 1;
        $user->verify_token = null;

        $user->save();

        event(new UserVerified($user));

        return true;
    }
}
<?php


namespace App\Services\RssFeed;


use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;

class Channel extends Fluent
{
    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->get('title');
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->get('link');
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->get('description');
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->get('language');
    }

    /**
     * @return string
     */
    public function getCopyright(): string
    {
        return $this->get('copyright');
    }

    /**
     * @return string
     */
    public function getGenerator(): string
    {
        return $this->get('generator');
    }

    /**
     * @return array
     */
    public function getImage(): array
    {
        return $this->get('image');
    }

    public function getLastBuildDate() : Carbon
    {
        return $this->get('lastBuildDate', Carbon::now());
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->get('articles');
    }
}
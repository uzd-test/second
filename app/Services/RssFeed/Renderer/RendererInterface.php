<?php


namespace App\Services\RssFeed\Renderer;


use App\Services\RssFeed\Article;
use App\Services\RssFeed\Channel;
use DOMElement;

interface RendererInterface
{
    /**
     * @param Article $article
     *
     * @return DOMElement
     */
    function renderArticle(Article $article): DOMElement;

    /**
     * @param Channel $channel
     *
     * @return string
     */
    function render(Channel $channel): string;
}
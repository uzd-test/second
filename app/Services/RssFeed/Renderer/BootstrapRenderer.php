<?php


namespace App\Services\RssFeed\Renderer;


use App\Services\RssFeed\Article;
use App\Services\RssFeed\Channel;
use DOMElement;
use Illuminate\Support\Str;

class BootstrapRenderer implements RendererInterface
{
    /**
     * @var \DOMDocument
     */
    protected $dom;

    public function __construct()
    {
        $impl = new \DOMImplementation();

        $this->dom = $impl->createDocument(null, 'html');
    }

    /**
     * @param Article $article
     *
     * @return DOMElement
     */
    function renderArticle(Article $article): DOMElement
    {
        $wrapper = $this->createElement('div', ['class' => 'card']);

        $wrapper->appendChild($this->createElement('img', [
            'class' => 'card-img-top',
            'src' => $article->getImageUrl()
        ]));

        $wrapper->appendChild($body = $this->createElement('div', ['class' => 'card-body']));

        $body->appendChild($this->createElement(
            'h5',
            ['class' => 'card-title', 'title' => $article->getTitle()],
            Str::words($article->getTitle(), 5)
        ));

        $body->appendChild($this->createElement(
            'p',
            ['class' => 'card-text'],
            Str::words(html_entity_decode($article->getDescription()), 20)
        ));

        $body->appendChild($this->createElement(
            'a',
            ['class' => 'btn btn-primary', 'href' => $article->getLink()],
            'View more'
        ));

        $wrapper->appendChild($footer = $this->createElement('div', ['class' => 'card-footer']));

        $footer->appendChild($this->createElement("small", ['class' => 'text-muted'], $article->getPubDate()));

        return $wrapper;
    }

    public function renderCopyright(Channel $channel): DOMElement
    {
        /**
         *
         * <div class="my-3 p-3 bg-white rounded shadow-sm">
         * <h6 class="border-bottom border-gray pb-2 mb-0">
         * RSS Feed
         * </h6>
         * <div class="media text-muted pt-3">
         * <img class="mr-2 rounded" style="height: 60px;" src="{{ $channel->getImage()['url'] }}">
         * <p class="media-body pb-3 mb-0 small lh-125">
         * <strong class="d-block text-gray-dark">{{ $channel->getTitle() }}</strong>
         * {{ $channel->getDescription() }}<br />
         * Copyright: {{ $channel->getCopyright() }}
         * </p>
         *
         * <a href="{{ $channel->getLink() }}" class="btn btn-primary float-right btn-sm" target="_blank">View source</a>
         * </div>
         * </div>
         */

        $wrapper = $this->createElement('div', ['class' => 'my-3 p-3 bg-white rounded shadow-sm"']);

        $wrapper->appendChild(
            $this->createElement('h6', ['class' => 'border-bottom border-gray pb-2 mb-0'], __('RSS Feed'))
        );

        $wrapper->appendChild($body = $this->createElement('div', ['class' => 'media text-muted pt-3']));

        if ($channel->getImage()) {
            $body->appendChild($this->createElement('img', [
                'class' => 'mr-2 rounded',
                'style' => 'height: 60px;',
                'src' => $channel->getImage()['url']
            ]));
        }

        $body->appendChild($mediaBody = $this->createElement('div', ['class' => "media-body pb-3 mb-0 small lh-125"]));

        $mediaBody->appendChild($this->createElement('strong', ['class' => 'd-block text-gray-dark'], $channel->getTitle()));
        $mediaBody->appendChild($this->createElement('div', [], $channel->getDescription()));
        $mediaBody->appendChild($this->createElement('div', [], __('Copyright: :copyright', ['copyright' => $channel->getCopyright()])));

        $body->appendChild($this->createElement('a', [
            'href' => $channel->getLink(),
            'class' => 'btn btn-primary float-right btn-sm',
            '_target' => 'blank'
        ], __('View source')));

        return $wrapper;
    }


    /**
     * @param Channel $channel
     *
     * @return string
     */
    function render(Channel $channel): string
    {
        $outer = $this->createElement('div');

        $wrapper = $this->dom->createElement('div');
        $wrapper->setAttribute('class', 'row');

        $outer->appendChild($this->renderCopyright($channel));
        $outer->appendChild($wrapper);


        foreach ($channel->getArticles() as $article) {

            $wrapper->appendChild(
                $col = $this->createElement(
                    "div",
                    ['class' => 'col-sm-3']
                )
            );

            $col->appendChild($this->renderArticle($article));
        }

        return $this->dom->saveHTML($outer);
    }

    /**
     * Create element shortcut
     *
     * @param string $name
     * @param array  $attributes
     * @param null   $body
     *
     * @return DOMElement
     */
    protected function createElement($name = 'div', array $attributes = [], $body = null)
    {
        $el = $this->dom->createElement($name, $body);

        foreach ($attributes as $k => $v) {
            $el->setAttribute($k, $v);
        }

        return $el;
    }
}
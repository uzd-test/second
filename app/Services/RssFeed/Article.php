<?php


namespace App\Services\RssFeed;


use Carbon\Carbon;
use Illuminate\Support\Fluent;

class Article extends Fluent
{
    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->get('title');
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->get('description');
    }


    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->get('link');
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->get('imageUrl');
    }

    /**
     * @return int
     */
    public function getCommentCount(): int
    {
        return $this->get('commentCount');
    }

    /**
     * @return Carbon
     */
    public function getPubDate(): Carbon
    {
        return $this->get('pubDate');
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->get('categories', []);
    }
}
<?php


namespace App\Http\Controllers\Auth;


use App\Exceptions\VerifyFailedException;
use App\Rules\UserNotVerified;
use App\Services\UserService;
use App\User;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

class VerifyController
{
    use ValidatesRequests;
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * VerifyController constructor.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function show()
    {
        $user = auth()->user();

        if($user->verified) {
            return abort(404);
        }

        return view('verify.verify');
    }

    public function verify(Request $request)
    {
        $userId = $request->route('user_id');
        $token = $request->route('token');

        try {
            if ($this->userService->verify($userId, $token)) {
                return redirect('/');
            }
        } catch(VerifyFailedException $e) {
            return redirect()->route('user.verify.show')->withErrors($e->getMessage());
        }
    }
}
<?php

namespace App\Http\Controllers;

use App\Services\RssFeed\Renderer\BootstrapRenderer;
use App\Services\RssFeedService;

class NewsController extends Controller
{
    /**
     * @var RssFeedService
     */
    protected $feedService;

    /**
     * NewsController constructor.
     *
     * @param RssFeedService $feedService
     */
    public function __construct(RssFeedService $feedService)
    {
        $this->feedService = $feedService;
    }

    public function index()
    {
        $channel = $this->feedService->getChannel();

        $renderer = new BootstrapRenderer();

        $output = $renderer->render($channel);

        return view('news.index', compact('output', 'channel'));
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class UserVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param string|null               $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($user = auth()->guard($guard)->user()) {
            if(!$user->verified) {
                return redirect('/auth/verify')->withErrors(__('You have to verify your account to proceed.'));
            }
        }

        return $next($request);
    }
}

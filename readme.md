# Requirements for this project
- PHP 7.2+
- MySQL 5.7
- DOM Extension for PHP
- Any other requirements for Laravel: See https://laravel.com/docs/5.6/installation


# Installation
- Clone project
- ```composer install```
- ```npm install```
- ```npm run build```
- ```php artisan migrate```
- Project is ready to go!
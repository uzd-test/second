$(function() {
    const $form = $('#register-form');

    $form.submit(async (e) => {
        e.preventDefault();

        var submit = $form.find('button[type=submit]');

        submit.addClass('disabled');

        var result = await validateEmailField($('#email'));

        submit.removeClass('disabled');

        if(result === true) {
            $form[0].submit();

            return true;
        }

        return alert('Invalid email provided');
    });

    $('#email').keyup(_.debounce( async (e) => {
        await validateEmailField( $(e.target) );
    }, 100));
});

async function validateEmailField(el) {
    let val = el.val();

    let $errors = el.siblings('.invalid-feedback');

    if($errors.length === 0) {
        $errors = el.after('<span class="invalid-feedback" role="alert" />');
    }

    const emailIsValid = validateEmail(val);

    el.removeClass('is-invalid');
    $errors.hide();

    let error = !emailIsValid ? 'Entered value is not an email' : null;

    if(!error) {
        let result = await checkEmailAvailability(val);

        if(result !== true) error = result || 'Invalid email provided';
    }

    if(error) {
        el.addClass('is-invalid');

        $errors.show();
        $errors.text(error);

        return false;
    }

    return true;
}

async function checkEmailAvailability(emailAddress) {
    try {
        var response = await fetch(`/api/user/availability?email=${emailAddress}`, {
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
        }).then(r => r.json());

        if(response.success) {
            return true;
        }

        if(_.has(response, 'errors.email')) {
            return response.errors.email[0];
        }

        return response.message;
    } catch(e) {

        return false;
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
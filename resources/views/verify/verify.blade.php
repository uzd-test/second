@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="alert alert-info">To complete registration you'll have to verify your email, we have sent a verification message to your email.</div>
    </div>
@endsection